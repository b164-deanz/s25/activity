db.fruits.aggregate([
     { $unwind: "$origin" },
     { $project:{name: 1} }
   
])

db.fruits.aggregate([
     { $match: {onSale: true} },
     { $count: "available_fruits" }
   
])

db.fruits.aggregate([
     { $match: {stock: {$gte: 20}} },
     { $count: "available_fruits" }
   
])

db.fruits.aggregate([
     { $match: {onSale: true}}, 
     { $unwind: "$origin"} ,    
     { $group: {_id: "$supplier_id", average: {$avg: "$price"}} }
   
])

db.fruits.aggregate([
     { $match: {onSale: true}}, 
     { $unwind: "$origin"} ,    
     { $group: {_id: "$supplier_id", average: {$max: "$price"}} }
   
])

db.fruits.aggregate([
     { $match: {onSale: true}}, 
     { $unwind: "$origin"} ,    
     { $group: {_id: "$supplier_id", average: {$min: "$price"}} }
   
])


db.fruits.aggregate([
    
     { $unwind: "$origin"} ,    
     { $group: {_id: "$origin", average: {$avg: "$supplier_id"}} }
   
])